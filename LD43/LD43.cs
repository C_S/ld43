﻿//Game created for the Ludum Dare 43 - 30-11-2018
//Tutorials from SixOfEleven used : http://gameprogrammingadventures.org
//Created by Quorusa - CS

using Characters;
using LD43.Map;
using LD43.ScreenManager;
using LD43.TileEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace LD43
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class main : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Character character;
        MapManager map;
        bool outOfBounds = false;

        GameScreen activeScreen;
        MenuScreen menuScreen;
        RulesScreen rulesScreen;
        PlayScreen playScreen;

        KeyboardState keyboardState;
        KeyboardState oldKeyboardState;

        Dictionary<AnimationKey, Animation> characterAnimations = new Dictionary<AnimationKey, Animation>();

        public Dictionary<AnimationKey, Animation> CharacterAnimations
        {
            get { return characterAnimations; }
        }

        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        public main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Set these values to the desired width and height of the window
            graphics.PreferredBackBufferWidth = 600;
            graphics.PreferredBackBufferHeight = 600;
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            Animation animation = new Animation(4, 50, 50, 0, 0);
            characterAnimations.Add(AnimationKey.WalkDown, animation);

            animation = new Animation(4, 50, 50, 0, 50);
            characterAnimations.Add(AnimationKey.WalkUp, animation);

            animation = new Animation(4, 50, 50, 0, 100);
            characterAnimations.Add(AnimationKey.WalkRight, animation);

            animation = new Animation(4, 50, 50, 0, 150);
            characterAnimations.Add(AnimationKey.WalkLeft, animation);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            character = new Character(this, "Toutouya", Content.Load<Texture2D>("Persos/SpritesSheet"));
            map = new MapManager(this, spriteBatch, character, false, 50, 50);

            // TODO: use this.Content to load your game content here
            menuScreen = new MenuScreen(
                this,
                spriteBatch,
                Content.Load<SpriteFont>("Fonts/menuFont"),
                Content.Load<Texture2D>("Menu/backgroundMenu"));
            Components.Add(menuScreen);
            menuScreen.Hide();

            rulesScreen = new RulesScreen(
                this,
                spriteBatch,
                Content.Load<Texture2D>("Menu/backgroundMenu"));
            Components.Add(rulesScreen);
            rulesScreen.Hide();

            playScreen = new PlayScreen(
                this,
                spriteBatch,
                character,
                Content.Load<Texture2D>("Menu/backgroundMenu"),
                map);
            Components.Add(playScreen);
            playScreen.Hide();

            activeScreen = menuScreen;
            activeScreen.Show();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            keyboardState = Keyboard.GetState();

            if (activeScreen == menuScreen)
            {
                if (CheckKey(Keys.Enter))
                {
                    if (menuScreen.SelectedIndex == 0)
                    {
                        activeScreen.Hide();
                        activeScreen = playScreen;
                    }

                    if (menuScreen.SelectedIndex == 1)
                    {
                        activeScreen.Hide();
                        activeScreen = rulesScreen;
                    }
                }
            }

            activeScreen.Show();

            if (activeScreen == playScreen)
            {
                Vector2 motion = Vector2.Zero;

                outOfBounds = character.OutOfBounds();

                Console.WriteLine("map.Walk {0}", map.walk);

                if (map.walk && !outOfBounds)
                {
                    if (keyboardState.IsKeyDown(Keys.W))
                    {
                        motion.Y -= 1;
                        character.Sprite.CurrentAnimation = AnimationKey.WalkUp;
                    }
                    if (keyboardState.IsKeyDown(Keys.S))
                    {
                        motion.Y += 1;
                        character.Sprite.CurrentAnimation = AnimationKey.WalkDown;
                    }
                    if (keyboardState.IsKeyDown(Keys.A))
                    {
                        motion.X -= 1;
                        character.Sprite.CurrentAnimation = AnimationKey.WalkLeft;
                    }
                    if (keyboardState.IsKeyDown(Keys.D))
                    {
                        motion.X += 1;
                        character.Sprite.CurrentAnimation = AnimationKey.WalkRight;
                    }

                    if (motion != Vector2.Zero)
                    {
                        motion.Normalize();
                        motion *= (character.Speed * (float)gameTime.ElapsedGameTime.TotalSeconds);

                        Vector2 newPos = character.Sprite.pos + motion;

                        character.Sprite.pos = newPos;
                        character.Sprite.IsAnimating = true;
                    }
                }

                outOfBounds = false;

            }

            // TODO: Add your update logic here

            base.Update(gameTime);

            activeScreen.Initialize();

            oldKeyboardState = keyboardState;
        }
        
        private bool CheckKey(Keys theKey)
        {
            return keyboardState.IsKeyUp(theKey) && oldKeyboardState.IsKeyDown(theKey);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            // TODO: Add your drawing code here

            base.Draw(gameTime);

            spriteBatch.End();

        }
    }
}
