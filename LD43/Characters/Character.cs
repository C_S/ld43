﻿using LD43;
using LD43.Map;
using LD43.TileEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Characters
{
    public class Character : DrawableGameComponent
    {
        private main gameRef;

        private string name;
        private AnimatedSprite sprite;
        private Texture2D text;
        private float speed = 180.0f;
        
        public AnimatedSprite Sprite
        {
            get { return sprite; }
        }

        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public Character(Game game, string name, Texture2D text) : base(game)
        {
            this.gameRef = (main)game;
            this.name = name;
            this.text = text;

            this.sprite = new AnimatedSprite(text, this.gameRef.CharacterAnimations);
            this.sprite.CurrentAnimation = AnimationKey.WalkDown;
        }


        //TO DO
        public Obstacle Destroy(Obstacle[,] obstacles)
        {
            int posCharacterX = (int)this.Sprite.pos.X / 50;
            int posCharacterY = (int)this.Sprite.pos.Y / 50;

            AnimationKey currentAnimation = this.Sprite.CurrentAnimation;
            
            foreach (Obstacle obst in obstacles)
            {
                switch (currentAnimation)
                {
                    default:
                        return new Obstacle(gameRef);
                    case AnimationKey.WalkDown:
                        if (posCharacterY+50 + 1 == obst.PosMap.Y && obst.Type != 2 && obst.Type != 3)
                            return obst;
                        break;
                    case AnimationKey.WalkUp:
                        if (posCharacterY- 1 == obst.PosMap.Y && obst.Type != 2 && obst.Type != 3)
                            return obst;
                        break;
                    case AnimationKey.WalkLeft:
                        if (posCharacterX- 1 == obst.PosMap.X && obst.Type != 2 && obst.Type != 3)
                            return obst;
                        break;
                    case AnimationKey.WalkRight:
                        if (posCharacterX+50 + 1 == obst.PosMap.X && obst.Type != 2 && obst.Type != 3)
                            return obst;
                        break;
                }
            }

            return new Obstacle(gameRef);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public bool OutOfBounds()
        {
            if ((this.Sprite.pos.X + this.Sprite.Width + 1) > gameRef.Window.ClientBounds.Width)
            {
                this.Sprite.pos.X -= 1;
                return true;
            }
            if ((this.Sprite.pos.Y + this.Sprite.Height + 1) > gameRef.Window.ClientBounds.Height)
            {
                this.Sprite.pos.Y -= 1;
                return true;
            }
            return false;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            sprite.Draw(gameTime, gameRef.SpriteBatch);
            
        }
    }
}
