﻿using LD43;
using LD43.TileEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Characters
{
    public class NPC : DrawableGameComponent
    {
        private main gameRef;

        private string name;
        private AnimatedSprite sprite;
        private Texture2D text;
        
        public AnimatedSprite Sprite
        {
            get { return sprite; }
        }
        
        public NPC(Game game, string name, Texture2D text) : base(game)
        {
            this.gameRef = (main)game;
            this.name = name;
            this.text = text;

            this.sprite = new AnimatedSprite(text, this.gameRef.CharacterAnimations);
            this.sprite.CurrentAnimation = AnimationKey.Idle;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            sprite.Draw(gameTime, gameRef.SpriteBatch);
        }
    }
}
