﻿using Characters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD43.Map
{
    public enum ObstacleType
    {
        Fire,
        Wood,
        Water,
        Metal,
        Dirt,
        Immortal,
        Free
    }

    class MapManager
    {
        Random rdm;
        main gameRef;
        SpriteBatch sb;
        public bool walk;

        public int cpt = 0;

        //Texture2D bg;
        Obstacle[,] obstacles;
        int[,] typeObstacles;
        int widthMap;
        int heightMap;
        bool sacrificeAcquired;
        int widthTile;
        int heightTile;
        private Character character;

        Texture2D text0, text1, text2, text3, text4, text5, text6;

        public List<ObstacleType> typeObst = new List<ObstacleType> { ObstacleType.Dirt, ObstacleType.Fire, ObstacleType.Free,
            ObstacleType.Immortal, ObstacleType.Metal, ObstacleType.Water, ObstacleType.Wood};

        public MapManager(Game game, SpriteBatch sb, Character character, bool sacrificeAcquired, int heightTile, int widthTile)
        {
            this.gameRef = (main)game;
            this.sb = sb;

            this.walk = true;

            this.widthMap = game.Window.ClientBounds.Width;
            this.heightMap = game.Window.ClientBounds.Height;
            this.sacrificeAcquired = sacrificeAcquired;
            this.character = character;

            this.widthTile = widthTile;
            this.heightTile = heightTile;
            
            text0 = gameRef.Content.Load<Texture2D>(String.Format("Obstacles/0"));
            text1 = gameRef.Content.Load<Texture2D>(String.Format("Obstacles/1"));
            text2 = gameRef.Content.Load<Texture2D>(String.Format("Obstacles/2"));
            text3 = gameRef.Content.Load<Texture2D>(String.Format("Obstacles/3"));
            text4 = gameRef.Content.Load<Texture2D>(String.Format("Obstacles/4"));
            text5 = gameRef.Content.Load<Texture2D>(String.Format("Obstacles/5"));
            text6 = gameRef.Content.Load<Texture2D>(String.Format("Obstacles/6"));

            obstacles = new Obstacle[widthMap / widthTile, heightMap / heightTile];
            typeObstacles = new int[widthMap / widthTile, heightMap / heightTile];

            rdm = new Random();


            for (int i = 0; i < widthMap / widthTile; i++)
            {
                for (int j = 0; j < heightMap / heightTile; j++)
                {
                    typeObstacles[i, j] = -1;
                }
            }

            for (int i = 0; i < widthMap / widthTile; i++)
            {
                for (int j = 0; j < heightMap / heightTile; j++)
                {
                    int typeObstChoose = 0;

                    if (i == 0 && j == 0)
                        typeObstChoose = 2;
                    else if (i == 1 && j == 0)
                        typeObstChoose = 2;
                    else if (i == 1 && j == 1)
                        typeObstChoose = 2;
                    else if (i == 0 && j == 1)
                        typeObstChoose = 2;
                    else
                        typeObstChoose = rdm.Next(0, typeObst.Count);

                    Texture2D text = new Texture2D(gameRef.GraphicsDevice, widthTile, heightTile);
                    Rectangle textRect = new Rectangle(i, j, widthTile, heightTile);

                    switch (typeObstChoose)
                    {
                        default:
                            text = text2;
                            break;
                        case 0:
                            text = text0;
                            break;
                        case 1:
                            text = text1;
                            break;
                        case 2:
                            text = text2;
                            break;
                        case 3:
                            text = text3;
                            break;
                        case 4:
                            text = text4;
                            break;
                        case 5:
                            text = text5;
                            break;
                        case 6:
                            text = text6;
                            break;
                    }

                    Obstacle obstacle = new Obstacle(gameRef, text, textRect, new Point(i, j), widthTile, heightTile, typeObstChoose == 5 ? true : false, typeObstChoose);

                    obstacles[i, j] = obstacle;

                    if (typeObstacles[i, j] == -1)
                    {
                        typeObstacles[i, j] = typeObstChoose;
                    }

                }
            }


            if (typeObstacles[0, 1] != 2 || typeObstacles[1, 1] != 2 || typeObstacles[1, 0] != 2)
                walk = false;
        }

        public void MapDraw(GameTime gameTime)
        {
            foreach (Obstacle obst in obstacles)
            {
                sb.Draw(obst.Text, new Rectangle(obst.PosMap.X, obst.PosMap.Y, 50, 50), Color.White);
            }
        }

        public void MapUpdate()
        {
            Rectangle hitRect = new Rectangle((int)character.Sprite.pos.X, (int)character.Sprite.pos.Y, character.Sprite.Width + 5, character.Sprite.Height + 5);

            foreach (Obstacle obst in obstacles)
            {
                if (!obst.Ghost)
                    walk = false;
            }

            foreach (Obstacle obst in obstacles)
            {
                if (obst.Type != 2 && obst.Type != 3 && hitRect.Intersects(obst.textRect))
                {
                    obst.Type = 2;
                    obst.Text = text2;
                }
            }
        
            //Obstacle obst = character.Destroy(obstacles);

            //if (!obst.Ghost)
            //    obst.Type = 2;
        }
        
    }
}
