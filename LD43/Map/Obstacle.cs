﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD43.Map
{
    
   public class Obstacle : DrawableGameComponent
    {
        Texture2D text;
        public Rectangle textRect;
        int width;
        int height;
        Point posMap;
        bool immortal;
        int type;

        private bool ghost;

        public Obstacle(Game game) : base(game)
        {
            ghost = true;
        }

        public Obstacle(Game game, Texture2D text, Rectangle textRect, Point posMap, int width, int height, bool immortal, int type) : base(game)
        {
            this.Text = text;
            this.width = width;
            this.height = height;
            this.textRect.X = textRect.X * width;
            this.textRect.Y = textRect.Y * height;
            this.posMap.X = posMap.X * width;
            this.posMap.Y = posMap.Y * height;
            this.immortal = immortal;
            this.type = type;
            this.ghost = immortal || type ==2 ? false : true;
        }

        public bool Ghost
        { get { return ghost; } }

        public int Type
        {
            get { return type; }
            set { type = value; }
        }

        public Point PosMap
        {
            get { return posMap; }
            set
            {
                this.posMap = value;
            }
        }

        public Texture2D Text
        {
            get { return text; }

            set
            {
                text = value;
            }
        }

        public virtual void Show()
        {
            this.Visible = true;
            this.Enabled = true;
        }

        public virtual void Hide()
        {
            this.Visible = false; 
            this.Enabled = false;
        }

    }
}
