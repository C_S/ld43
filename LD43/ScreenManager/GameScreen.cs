﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD43.ScreenManager
{
    class GameScreen : DrawableGameComponent
    {
        List<GameComponent> components = new List<GameComponent>();

        protected Game game;
        protected SpriteBatch sb;

        public List<GameComponent> Components
        {
            get { return components; }
        }

        public GameScreen(Game game, SpriteBatch sb) : base(game)
        {
            this.game = game;
            this.sb = sb;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            foreach (GameComponent gc in components)
            {
                if (gc.Enabled == true)
                    gc.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            foreach (GameComponent gc in components)
            {
                if (gc is DrawableGameComponent && ((DrawableGameComponent)gc).Visible)
                    ((DrawableGameComponent)gc).Draw(gameTime);
            }
        }

        public virtual void Show()
        {
            this.Visible = true;
            this.Enabled = true;

            foreach (GameComponent gc in components)
            {
                gc.Enabled = true;
                if (gc is DrawableGameComponent)
                    ((DrawableGameComponent)gc).Visible = true;
            }
        }

        public virtual void Hide()
        {
            this.Visible = false;
            this.Enabled = false;

            foreach (GameComponent gc in components)
            {
                gc.Enabled = false;
                if (gc is DrawableGameComponent)
                    ((DrawableGameComponent)gc).Visible = false;
            }
        }


    }
}
