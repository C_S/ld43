﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD43.ScreenManager
{
    class MenuComponent : DrawableGameComponent
    {
        string[] menuElements;
        int selectedIndex;

        Color basicColor = Color.Black;
        Color flashColor = Color.Blue;

        Texture2D texture;

        KeyboardState keyboardState;
        KeyboardState oldKeyboardState;

        SpriteBatch sb;
        SpriteFont sf;

        Vector2 pos;
        float width = 0f;
        float height = 0f;

        public MenuComponent(Game game, SpriteBatch sb, SpriteFont sf, string[] menuElements, Texture2D texture) : base(game)
        {
            this.sb = sb;
            this.sf = sf;
            this.menuElements = menuElements;
            this.texture = texture;

            MeasureMenu();

            pos = new Vector2(15, 15);
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                if (selectedIndex < 0)
                    selectedIndex = 0;
                if (selectedIndex >= menuElements.Length)
                    selectedIndex = menuElements.Length - 1;
            }
        }


        private void MeasureMenu()
        {
            height = 0f;
            width = texture.Width;

            foreach (string element in menuElements)
            {
                Vector2 sizeSf = sf.MeasureString(element);

                if (sizeSf.X > width)
                    width = sizeSf.X;

                if (sizeSf.Y > height)
                    height = sizeSf.Y;

                if (menuElements.Count() > 1)
                    height += texture.Height + 50;
            }

            height -= 50;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        private bool CheckKey(Keys theKey)
        {
            return keyboardState.IsKeyUp(theKey) && oldKeyboardState.IsKeyDown(theKey);
        }

        public override void Update(GameTime gameTime)
        {
            keyboardState = Keyboard.GetState();

            Vector2 menuPos = pos;

            Rectangle elementRect;

            for (int i = 0; i < menuElements.Count(); i++)
            {
                elementRect = new Rectangle((int)menuPos.X, (int)menuPos.Y, texture.Width, texture.Height);

                menuPos.Y += texture.Height + 65;
            }

            if (CheckKey(Keys.Down))
            {
                selectedIndex++;
                if (selectedIndex == menuElements.Length)
                    selectedIndex = 0;
            }

            if (CheckKey(Keys.Up))
            {
                selectedIndex--;
                if (selectedIndex < 0)
                    selectedIndex = menuElements.Length - 1;
            }

            base.Update(gameTime);

            oldKeyboardState = keyboardState;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            Vector2 menuPos = pos;
            Color color;

            for (int i = 0; i < menuElements.Length; i++)
            {
                if (i == selectedIndex)
                    color = flashColor;
                else
                    color = basicColor;

                sb.Draw(texture, menuPos, Color.White);

                Vector2 textSize = sf.MeasureString(menuElements[i]);

                Vector2 textPos = menuPos + new Vector2((int)(texture.Width - textSize.X)/2, (int)(texture.Height - textSize.Y)/2);
                
                sb.DrawString(
                    sf,
                    menuElements[i],
                    textPos,
                    color);
                menuPos.Y += texture.Height + 50;
            }
        }


    }
}
