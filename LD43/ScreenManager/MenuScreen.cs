﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD43.ScreenManager
{
    class MenuScreen : GameScreen
    {
        MenuComponent mc;
        Texture2D bg;
        Rectangle bgRect;

        public int SelectedIndex
        {
            get { return mc.SelectedIndex; }
            set { mc.SelectedIndex = value; }
        }

        public MenuScreen(Game game, SpriteBatch sb, SpriteFont sf, Texture2D bg) : base(game, sb)
        {
            string[] menuElements = { "Start", "Rules"};

            mc = new MenuComponent(
                game,
                sb,
                sf,
                menuElements,
                game.Content.Load<Texture2D>("Buttons/backgroundButton"));
            Components.Add(mc);

            this.bg = bg;

            bgRect = new Rectangle(0,
                0,
                Game.Window.ClientBounds.Width,
                Game.Window.ClientBounds.Height);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            sb.Draw(bg, bgRect, Color.White);
            base.Draw(gameTime);
        }
    }
}
