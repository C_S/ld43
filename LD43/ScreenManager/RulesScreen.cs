﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD43.ScreenManager
{
    class RulesScreen : GameScreen
    {
        Texture2D bg;
        Rectangle bgRect;

        public RulesScreen(Game game, SpriteBatch sb, Texture2D bg) : base(game, sb)
        {
            this.bg = bg;
            this.bgRect = new Rectangle(0, 0, 600, 600);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            sb.Draw(bg, bgRect, Color.White);
            base.Draw(gameTime);
        }
    }
}
