﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Characters;
using LD43.Map;

namespace LD43.ScreenManager
{
    class PlayScreen : GameScreen
    {
        KeyboardState keyboardState;
        Rectangle imgRect;
        Texture2D text;
        
        public Character character;
        public MapManager map;

        public PlayScreen(Game game, SpriteBatch sb, Character character, Texture2D text, MapManager map) : base(game, sb)
        {
            imgRect = new Rectangle(
                0,
                0,
                Game.Window.ClientBounds.Width,
                Game.Window.ClientBounds.Height);
            this.character = character;
            this.text = text;
            this.map = map;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            keyboardState = Keyboard.GetState();

            character.Sprite.pos.X = Math.Min(Math.Max(0, character.Sprite.pos.X), game.Window.ClientBounds.Width);
            character.Sprite.pos.Y = Math.Min(Math.Max(0, character.Sprite.pos.Y), game.Window.ClientBounds.Height);

            map.MapUpdate();

            character.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            sb.Draw(text, imgRect, Color.White);
            map.MapDraw(gameTime);
            character.Draw(gameTime);

        }
    }
}
